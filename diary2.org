# -*- fill-column: 79 -*-
#+AUTHOR: thunk
#+TITLE: Path of Exile 2 Build Diary
#+CATEGORY diary
#+EMAIL: 876495-thunk@users.noreply.gitlab.com
#+STARTUP: indent hidestars show2levels

#+BEGIN_SRC elisp tz
(setq org-time-stamp-formats '("<%Y-%m-%d %a>" . "<%Y-%m-%d %a %H:%M %z>"))
#+END_SRC

#+begin_src emacs-lisp default time-stamps
    (setq-local org-time-stamp-formats
                '("%Y-%m-%d %a" . "%Y-%m-%d %a %H:%M"))
#+end_src

* *scratch*

#+BEGIN_SRC bat
#+END_SRC
